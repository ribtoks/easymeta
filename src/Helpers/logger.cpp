
/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "logger.h"

#include <iostream>

#include <QDir>
#include <QFile>
#include <QFlags>
#include <QIODevice>
#include <QMutex>
#include <QMutexLocker>
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QtGlobal>

#include "Common/defines.h"
#include "Common/logging.h"
#include "Encryption/obfuscation.h"

void myMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    Helpers::Logger &logger = Helpers::Logger::getInstance();
    logger.log(type, context, msg);

#if defined(INTEGRATION_TESTS) || defined(UI_TESTS)
    if ((type == QtFatalMsg) || (type == QtCriticalMsg) || (type == QtWarningMsg)) { logger.abortFlush(); }
#endif

    if (type == QtFatalMsg) { abort(); }
}

namespace Helpers {
    void Logger::log(QtMsgType type, const QMessageLogContext &context, const QString &message) {
        QString logLine = qFormatLogMessage(type, context, message);
        doLog(logLine);

        if (type == QtWarningMsg && message.endsWith(m_ReportTag)) { sendMessage(message); }
    }

    void Logger::flush() {
        QMutexLocker flushLocker(&m_FlushMutex);

        while (m_QueueFlushFrom->isEmpty()) {
            QMutexLocker logLocker(&m_LogMutex);

            if (m_QueueLogTo->isEmpty()) {
                m_AnyLogsToFlush.wait(&m_LogMutex);
            } else {
                qSwap(m_QueueLogTo, m_QueueFlushFrom);
            }
        }

        Q_ASSERT(m_QueueFlushFrom->length() > 0);
        flushStream(m_QueueFlushFrom);
    }

    void Logger::emergencyLog(const char *const message) {
#ifdef WITH_LOGS
        if (!m_MemoryOnly && !m_LogFilepath.isEmpty()) {
            QFile outFile(m_LogFilepath);
            if (outFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
                outFile.write(message);
                outFile.flush();
            }
        }
#endif

        std::cerr << message << std::endl;
        std::cerr.flush();
    }

    void Logger::emergencyFlush() {
        flushStream(&m_LogsStorage[0]);
        flushStream(&m_LogsStorage[1]);
    }

    void Logger::stop() {
        // will make waiting flush() call unblocked if any
        doLog("Logging stopped.");
    }

#if defined(INTEGRATION_TESTS) || defined(UI_TESTS)
    void Logger::abortFlush() {
        doLog("Starting abort flush.");
        flushAll();
    }
#endif

    void Logger::doLog(const QString &message) {
        QMutexLocker locker(&m_LogMutex);
        m_QueueLogTo->append(message);
        m_AnyLogsToFlush.wakeOne();
    }

    QString Logger::prepareLine(const QString &lineToWrite) {
        QString line = lineToWrite;
        if (line.endsWith(m_ReportTag)) { line.chop(m_ReportTag.size()); }

#ifdef QT_DEBUG
        return line;
#else
        return Encryption::rot13plus(line);
#endif
    }

    void Logger::flushAll() {
        QMutexLocker flushLocker(&m_FlushMutex);
        flushStream(m_QueueFlushFrom);

        {
            QMutexLocker logLocker(&m_LogMutex);

            if (!m_QueueLogTo->isEmpty()) { qSwap(m_QueueLogTo, m_QueueFlushFrom); }
        }

        flushStream(m_QueueFlushFrom);
    }

    void Logger::flushStream(QStringList *logItems) {
        Q_ASSERT(logItems != nullptr);
        if (logItems->empty()) { return; }

#ifdef WITH_LOGS
        if (!m_MemoryOnly && !m_LogFilepath.isEmpty()) {
            QFile outFile(m_LogFilepath);
            if (outFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)) {
                QTextStream ts(&outFile);
                ts.setCodec("UTF-8");

                int size = logItems->size();
                for (int i = 0; i < size; ++i) {
                    const QString &line = logItems->at(i);
                    QString prepared    = prepareLine(line);
                    ts << prepared;
    #if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
                    Qt::endl(ts);
    #else
                    endl(ts);
    #endif
                }
            }
        }
#endif

#ifdef WITH_STDOUT_LOGS
        int size = logItems->size();
        for (int i = 0; i < size; ++i) {
            const QString &line = logItems->at(i);
            std::cerr << line.toLocal8Bit().data() << std::endl;
        }

        std::cerr.flush();
#endif

        logItems->clear();
    }

    Logger::Logger(): m_ReportTag(REPORT_ME), m_MemoryOnly(false) {
        m_QueueLogTo     = &m_LogsStorage[0];
        m_QueueFlushFrom = &m_LogsStorage[1];

#if (QT_VERSION >= QT_VERSION_CHECK(5, 4, 0))
        qSetMessagePattern("%{time hh:mm:ss.zzz} %{type} T#%{threadid} %{function} - %{message}");
#endif

#ifdef CORE_TESTS
        m_MemoryOnly = true;
#endif

        qInstallMessageHandler(myMessageHandler);
    }

    Logger::~Logger() {
        qInstallMessageHandler(nullptr);
        flushAll();
    }
}  // namespace Helpers
