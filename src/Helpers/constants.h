/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef CONSTANTS_H
#define CONSTANTS_H

const char ID_KEY[]          = "id";
const char TYPE_KEY[]        = "type";
const char FILES_KEY[]       = "files";
const char BACKUPS_KEY[]     = "backups";
const char WITH_PROXY_KEY[]  = "with-proxy";
const char WITH_DIRECT_KEY[] = "with-direct";
const char OUT_DIR_KEY[]     = "out-dir";

const char READ_TYPE[]      = "read";
const char RESTORE_TYPE[]   = "restore";
const char WRITE_TYPE[]     = "write";
const char WIPE_TYPE[]      = "wipe";
const char THUMBNAIL_TYPE[] = "thumb";

const char ERROR_KEY[]        = "error";
const char SOURCE_FILE_KEY[]  = "SourceFile";
const char TITLE_KEY[]        = "Title";
const char DESCRIPTION_KEY[]  = "Description";
const char COPYRIGHT_KEY[]    = "Copyright";
const char HEADLINE_KEY[]     = "Headline";
const char KEYWORDS_KEY[]     = "Keywords";
const char EXTENSION_KEY[]    = "Extension";
const char OUT_FILENAME_KEY[] = "OutFilename";
const char CREATED_AT_KEY[]   = "CreatedAt";

#endif  // CONSTANTS_H
