#include "pnghelpers.h"

#include <cstdio>
#include <cstring>

#include <QString>

#include "Common/logging.h"

#include "png.h"

std::vector<std::pair<std::string, std::string>> readPNGMetadata(const QString &filepath) {
    FILE *file = nullptr;
#ifdef Q_OS_WIN
    file = _wfopen(filepath.toStdWString().c_str(), L"rb");
#else
    file = fopen(filepath.toStdString().c_str(), "rb");
#endif
    if (file == nullptr) {
        LOG_WARNING << "Failed to open the file:" << PII(filepath);
        return {};
    }

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
    if (!png) {
        LOG_WARNING << "Error creating PNG read struct";
        fclose(file);
        return {};
    }

    png_infop info = png_create_info_struct(png);
    if (!info) {
        LOG_WARNING << "Error creating PNG info struct";
        png_destroy_read_struct(&png, nullptr, nullptr);
        fclose(file);
        return {};
    }

    if (setjmp(png_jmpbuf(png))) {
        LOG_WARNING << "Error setting jump buffer";
        png_destroy_read_struct(&png, &info, nullptr);
        fclose(file);
        return {};
    }

    png_init_io(png, file);
    png_read_info(png, info);

    using str_pair = std::pair<std::string, std::string>;
    std::vector<str_pair> metadata;

    // taken from png_textp "spec"/comment
    const size_t MAX_METADATA_KEY_LEN = 79;

    png_textp text_ptr {};
    int num_text = 0;
    if (png_get_text(png, info, &text_ptr, &num_text) > 0) {
        for (int i = 0; i < num_text; ++i) {
            auto &text = text_ptr[i];
            if ((nullptr == text.key) || (nullptr == text.text)) { continue; }

            const size_t keyLength = strnlen(text.key, MAX_METADATA_KEY_LEN);
            std::string key(text.key, keyLength);
            if (key.empty()) { continue; }
            if (0 == text.text_length) { continue; }

            std::string value(text.text, text.text_length);
            if (value.empty()) { continue; }

            metadata.emplace_back(str_pair(std::move(key), std::move(value)));
        }

        LOG_INFO << "Read" << metadata.size() << "metadata pairs from PNG";
    }

    png_destroy_read_struct(&png, &info, nullptr);
    fclose(file);

    return metadata;
}
