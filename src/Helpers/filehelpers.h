/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef FILEHELPERS_H
#define FILEHELPERS_H

#include <QString>

class QFile;

namespace Helpers {
    bool removeFile(const QString &fullpath, int retries);
    bool copyToProxy(const QString &filepath, qint64 maxSize, QFile &proxy);
    bool renameFile(const QString &from, const QString &to);
}

#endif  // FILEHELPERS_H
