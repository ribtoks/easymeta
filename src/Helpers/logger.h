/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <atomic>

#include <QMutex>
#include <QString>
#include <QStringList>
#include <QWaitCondition>

#include "Common/messages.h"

namespace Helpers {
    class Logger: public Common::MessagesSource<QString>
    {
    public:
        static Logger &getInstance() {
            static Logger instance;  // Guaranteed to be destroyed.
            // Instantiated on first use.
            return instance;
        }

    public:
        void setLogFilePath(const QString &filepath) { m_LogFilepath = filepath; }

        void setMemoryOnly(bool value) { m_MemoryOnly = value; }

        QString getLogFilePath() const { return m_LogFilepath; }

        void log(QtMsgType type, const QMessageLogContext &context, const QString &message);
        void flush();
        void emergencyLog(const char *const message);
        void emergencyFlush();
        void stop();

#if defined(INTEGRATION_TESTS) || defined(UI_TESTS)
    public:
        void abortFlush();
#endif

    private:
        void doLog(const QString &message);
        QString prepareLine(const QString &lineToWrite);
        void flushAll();
        void flushStream(QStringList *logItems);

    private:
        Logger();
        ~Logger();

        Logger(Logger const &);
        void operator=(Logger const &);

    private:
        QString m_LogFilepath;
        QStringList m_LogsStorage[2];
        QStringList *m_QueueFlushFrom;
        QStringList *m_QueueLogTo;
        QLatin1String m_ReportTag;
        QMutex m_LogMutex;
        QMutex m_FlushMutex;
        QWaitCondition m_AnyLogsToFlush;
        std::atomic_bool m_MemoryOnly;
    };
}  // namespace Helpers

#endif  // LOGGER_H
