/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "loggingworker.h"

#include <QThread>

#include "Helpers/logger.h"

namespace Helpers {
    LoggingWorker::LoggingWorker(QObject *parent): QObject(parent), m_Cancel(false) { }

    void LoggingWorker::process() {
        Logger &logger         = Logger::getInstance();
        const int sleepTimeout = 700;

        while (!m_Cancel) {
            logger.flush();
            QThread::msleep(sleepTimeout);
        }

        emit stopped();
    }

    void LoggingWorker::cancel() {
        m_Cancel = true;

        Logger &logger = Logger::getInstance();
        logger.stop();
    }
}  // namespace Helpers
