/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef EXIV2IOHELPERS_H
#define EXIV2IOHELPERS_H

#include <QDateTime>
#include <QString>
#include <QStringList>

#include "Common/flags.h"

class Exiv2InitHelper
{
public:
    Exiv2InitHelper();
    ~Exiv2InitHelper();
};

struct BasicMetadata
{
    QString m_Title;
    QString m_Description;
    QString m_Copyright;
    QString m_Headline;
    QStringList m_Keywords;
    QDateTime m_CreatedAt {};
    Common::MetadataFlags m_UseFlags = Common::MetadataFlags::None;
};

bool readMetadata(const QString &filepath, BasicMetadata &importResult);
bool writeMetadata(const QString &filepath, const BasicMetadata &metadata);
bool wipeMetadata(const QString &filepath);
bool createThumbnail(const QString &filepath, const QString &outPath, QString &extension);

#endif  // EXIV2IOHELPERS_H
