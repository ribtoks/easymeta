/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "exiv2iohelpers.h"

#include <cstring>
#include <list>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include <QDate>
#include <QDateTime>
#include <QDebug>
#include <QLatin1String>
#include <QList>
#include <QStringList>
#include <QTextCodec>
#include <QTime>
#include <Qt>
#include <QtGlobal>

#ifdef Q_OS_WIN
    #include <Windows.h>
#endif
#include <exiv2/exiv2.hpp>

#include "Common/logging.h"
#include "Helpers/pnghelpers.h"

#define X_DEFAULT QString::fromLatin1("x-default")

const QLatin1String XMP_DESCRIPTION("Xmp.dc.description");
const QLatin1String XMP_PS_HEADLINE("Xmp.photoshop.Headline");
const QLatin1String XMP_TITLE("Xmp.dc.title");
const QLatin1String XMP_DATE_CREATED("Xmp.xmp.CreateDate");
const QLatin1String XMP_KEYWORDS("Xmp.dc.subject");
const QLatin1String XMP_COPYRIGHT("Xmp.dc.rights");

const QLatin1String IPTC_DESCRIPTION("Iptc.Application2.Caption");
const QLatin1String IPTC_TITLE("Iptc.Application2.ObjectName");
const QLatin1String IPTC_COPYRIGHT("Iptc.Application2.Copyright");
const QLatin1String IPTC_HEADLINE("Iptc.Application2.Headline");
const QLatin1String IPTC_KEYWORDS("Iptc.Application2.Keywords");
const QLatin1String IPTC_DATE_CREATED("Iptc.Application2.DateCreated");
const QLatin1String IPTC_DIGITAL_DATE_CREATED("Iptc.Application2.DigitizationDate");
const QLatin1String IPTC_TIME_CREATED("Iptc.Application2.TimeCreated");
const QLatin1String IPTC_DIGITAL_TIME_CREATED("Iptc.Application2.DigitizationTime");
const QLatin1String IPTC_CHARSET("Iptc.Envelope.CharacterSet");
const QLatin1String IPTC_CHARSET_UTF8("\033%G");

const QLatin1String EXIF_USERCOMMENT("Exif.Photo.UserComment");
const QLatin1String EXIF_DESCRIPTION("Exif.Image.ImageDescription");
const QLatin1String EXIF_XP_SUBJECT("Exif.Image.XPSubject");
const QLatin1String EXIF_XP_TITLE("Exif.Image.XPTitle");
const QLatin1String EXIF_XP_KEYWORDS("Exif.Image.XPKeywords");
const QLatin1String EXIF_COPYRIGHT("Exif.Image.Copyright");
const QLatin1String EXIF_PHOTO_DATETIMEORIGINAL("Exif.Photo.DateTimeOriginal");
const QLatin1String EXIF_IMAGE_DATETIMEORIGINAL("Exif.Image.DateTimeOriginal");

const int IPTC_COPYRIGHT_MAX_LENGTH = 128;
const int IPTC_HEADLINE_MAX_LENGTH  = 256;

Exiv2InitHelper::Exiv2InitHelper() { Exiv2::XmpParser::initialize(); }

Exiv2InitHelper::~Exiv2InitHelper() { Exiv2::XmpParser::terminate(); }

// ----------------------------------------------------------------------

// helper from libkexiv2
static bool isUtf8(const char *const buffer) {
    int i, n;
    unsigned char c;
    bool gotone = false;

    if (!buffer) { return true; }

    // character never appears in text
#define F 0
    // character appears in plain ASCII text
#define T 1
    // character appears in ISO-8859 text
#define I 2
    // character appears in non-ISO extended ASCII (Mac, IBM PC)
#define X 3

    static const unsigned char text_chars[256] = {
        //                  BEL BS HT LF    FF CR
        F, F, F, F, F, F, F, T, T, T, T, F, T, T, F, F,  // 0x0X
        //                              ESC
        F, F, F, F, F, F, F, F, F, F, F, T, F, F, F, F,  // 0x1X
        T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,  // 0x2X
        T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,  // 0x3X
        T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,  // 0x4X
        T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,  // 0x5X
        T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T,  // 0x6X
        T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, F,  // 0x7X
        //            NEL
        X, X, X, X, X, T, X, X, X, X, X, X, X, X, X, X,  // 0x8X
        X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X,  // 0x9X
        I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I,  // 0xaX
        I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I,  // 0xbX
        I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I,  // 0xcX
        I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I,  // 0xdX
        I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I,  // 0xeX
        I, I, I, I, I, I, I, I, I, I, I, I, I, I, I, I   // 0xfX
    };

    for (i = 0; (c = buffer[i]); ++i) {
        if ((c & 0x80) == 0) {
            // 0xxxxxxx is plain ASCII

            // Even if the whole file is valid UTF-8 sequences,
            // still reject it if it uses weird control characters.

            if (text_chars[c] != T) { return false; }

        } else if ((c & 0x40) == 0) {
            // 10xxxxxx never 1st byte
            return false;
        } else {
            // 11xxxxxx begins UTF-8
            int following = 0;

            if ((c & 0x20) == 0) {
                // 110xxxxx
                following = 1;
            } else if ((c & 0x10) == 0) {
                // 1110xxxx
                following = 2;
            } else if ((c & 0x08) == 0) {
                // 11110xxx
                following = 3;
            } else if ((c & 0x04) == 0) {
                // 111110xx
                following = 4;
            } else if ((c & 0x02) == 0) {
                // 1111110x
                following = 5;
            } else {
                return false;
            }

            for (n = 0; n < following; ++n) {
                i++;

                if (!(c = buffer[i])) { goto done; }

                if ((c & 0x80) == 0 || (c & 0x40)) { return false; }
            }

            gotone = true;
        }
    }

done:
    return gotone;  // don't claim it's UTF-8 if it's all 7-bit.
}

#undef F
#undef T
#undef I
#undef X

// copy-paste code from libkexiv2
static QString detectEncodingAndDecode(const std::string &value) {
    if (value.empty()) { return QString(); }

    if (isUtf8(value.c_str())) { return QString::fromUtf8(value.c_str()); }

    // Utf8 has a pretty unique byte pattern.
    // That's not true for ASCII, it is not possible
    // to reliably autodetect different ISO-8859 charsets.
    // So we can use either local encoding, or latin1.

    //TODO: KDE4PORT: check for regression of #134999 (very probably no regression!)
    return QString::fromLocal8Bit(value.c_str());
}

// ----------------------------------------------------------------------

static QStringList decomposeKeyword(const QString &keyword) {
    QStringList keywords = keyword.split(',',
#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
                                         Qt::SkipEmptyParts
#else
                                         QString::SkipEmptyParts
#endif
    );
    for (QString &kw: keywords) {
        kw = kw.trimmed();
    }
    return keywords;
}

QString getIptcCharset(Exiv2::IptcData &iptcData) {
    QString iptcCharset = "";

    try {
        const char *charsetPtr = iptcData.detectCharset();
        if (charsetPtr != nullptr) { iptcCharset = QString::fromLatin1(charsetPtr).toUpper(); }
    } catch (Exiv2::Error &e) { LOG_WARNING << "Exiv2 error:" << e.what(); }

    return iptcCharset;
}

static bool getXmpLangAltValue(Exiv2::XmpData &xmpData,
                               const QLatin1String &propertyName,
                               const QString &langAlt,
                               QString &resultValue) {
    bool anyFound = false;

    Exiv2::XmpKey key(propertyName.data());
    Exiv2::XmpData::iterator it = xmpData.findKey(key);
    if ((it != xmpData.end()) && (it->typeId() == Exiv2::langAlt)) {
        const Exiv2::LangAltValue &value = static_cast<const Exiv2::LangAltValue &>(it->value());
        LOG_DEBUG << "Found XMP Alt value for" << propertyName;
        QString anyValue;

        Exiv2::LangAltValue::ValueType::const_iterator it2 = value.value_.begin();
        Exiv2::LangAltValue::ValueType::const_iterator end = value.value_.end();
        for (; it2 != end; ++it2) {
            QString lang = QString::fromUtf8(it2->first.c_str());

            if (langAlt == lang) {
                QString text = QString::fromUtf8(it2->second.c_str()).trimmed();
                if (!text.isEmpty()) {
                    anyFound    = true;
                    resultValue = text.trimmed();
                    break;
                }
            }

            if (anyValue.isEmpty()) {
                QString text = QString::fromUtf8(it2->second.c_str());
                anyValue     = text.trimmed();
            }
        }

        if (!anyFound && !anyValue.isEmpty()) {
            anyFound    = true;
            resultValue = anyValue;
        }
    }

    return anyFound;
}

static bool getXmpDescription(Exiv2::XmpData &xmpData, const QString &langAlt, QString &description) {
    bool anyFound = false;

    try {
        anyFound = getXmpLangAltValue(xmpData, XMP_DESCRIPTION, langAlt, description);

        if (!anyFound || description.isEmpty()) {
            Exiv2::XmpKey psKey(XMP_PS_HEADLINE.data());
            Exiv2::XmpData::iterator xmpIt = xmpData.findKey(psKey);
            if (xmpIt != xmpData.end()) {
                const Exiv2::XmpTextValue &value = static_cast<const Exiv2::XmpTextValue &>(xmpIt->value());
                QString headline                 = QString::fromUtf8(value.value_.c_str()).trimmed();

                if (!headline.isEmpty()) {
                    LOG_DEBUG << "Found XMP PS Headline";
                    anyFound    = true;
                    description = headline;
                }
            }
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getXmpTitle(Exiv2::XmpData &xmpData, const QString &langAlt, QString &title) {
    bool anyFound = false;

    try {
        anyFound = getXmpLangAltValue(xmpData, XMP_TITLE, langAlt, title);
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getXmpCopyright(Exiv2::XmpData &xmpData, const QString &langAlt, QString &copyright) {
    bool anyFound = false;

    try {
        anyFound = getXmpLangAltValue(xmpData, XMP_COPYRIGHT, langAlt, copyright);
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getXmpTagStringBag(Exiv2::XmpData &xmpData, const QLatin1String &propertyName, QStringList &bag) {
    bool found = false;
    bool anyAdded = false;

    Exiv2::XmpKey key(propertyName.data());
    Exiv2::XmpData::iterator it = xmpData.findKey(key);

    if ((it != xmpData.end()) && (it->typeId() == Exiv2::xmpBag)) {
        found     = true;
        int count = it->count();
        bag.reserve(count);

        if (count == 1) {
            QString bagValue = QString::fromUtf8(it->toString(0).c_str());
            if (bagValue.contains(',')) {
                LOG_WARNING << "processing legacy saved XMP keywords";
                QStringList decomposed = decomposeKeyword(bagValue);
                if (!decomposed.empty()) {
                    bag += decomposed;
                    anyAdded = true;
                }
            } else {
                bag.append(bagValue);
                anyAdded = true;
            }
        } else {
            for (int i = 0; i < count; i++) {
                QString bagValue = QString::fromUtf8(it->toString(i).c_str());
                bag.append(bagValue);
                anyAdded = true;
            }
        }
    }

    return found && anyAdded;
}

static bool getXmpDateTime(Exiv2::XmpData &xmpData, QDateTime &dateTime) {
    bool anyFound = false;

    try {
        Exiv2::XmpKey psKey(XMP_DATE_CREATED.data());
        Exiv2::XmpData::iterator xmpIt = xmpData.findKey(psKey);

        if (xmpIt != xmpData.end()) {
            dateTime = QDateTime::fromString(QString::fromLatin1(xmpIt->toString().c_str()), Qt::ISODate);
            anyFound = dateTime.isValid();
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getXmpKeywords(Exiv2::XmpData &xmpData, QStringList &keywords) {
    bool anyFound = false;

    try {
        anyFound = getXmpTagStringBag(xmpData, XMP_KEYWORDS, keywords);
        if (anyFound) { LOG_DEBUG << "Found" << keywords.size() << "XMP keywords"; }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool
getIptcString(Exiv2::IptcData &iptcData, const QLatin1String &propertyName, bool isIptcUtf8, QString &resultValue) {
    bool anyFound = false;

    Exiv2::IptcKey key(propertyName.data());

    Exiv2::IptcData::iterator it = iptcData.findKey(key);
    if (it != iptcData.end()) {
        std::ostringstream os;
        os << *it;
        std::string str = os.str();
        LOG_DEBUG << "Found IPTC string for" << propertyName << "size:" << str.size();

        QString value;

        if (isIptcUtf8 || isUtf8(str.c_str())) {
            value = QString::fromUtf8(str.c_str()).trimmed();
        } else {
            value = QString::fromLocal8Bit(str.c_str()).trimmed();
        }

        if (!value.isEmpty()) {
            resultValue = value;
            anyFound    = true;
        }
    }

    return anyFound;
}

static bool getIptcDescription(Exiv2::IptcData &iptcData, bool isIptcUtf8, QString &description) {
    bool anyFound = false;

    try {
        anyFound = getIptcString(iptcData, IPTC_DESCRIPTION, isIptcUtf8, description);
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getIptcTitle(Exiv2::IptcData &iptcData, bool isIptcUtf8, QString &title) {
    bool anyFound = false;

    try {
        anyFound = getIptcString(iptcData, IPTC_TITLE, isIptcUtf8, title);
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getIptcCopyright(Exiv2::IptcData &iptcData, bool isIptcUtf8, QString &copyright) {
    bool anyFound = false;

    try {
        anyFound = getIptcString(iptcData, IPTC_COPYRIGHT, isIptcUtf8, copyright);
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getIptcHeadline(Exiv2::IptcData &iptcData, bool isIptcUtf8, QString &headline) {
    bool anyFound = false;

    try {
        anyFound = getIptcString(iptcData, IPTC_HEADLINE, isIptcUtf8, headline);
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyFound = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyFound = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyFound;
}

static bool getIptcKeywords(Exiv2::IptcData &iptcData, bool isIptcUtf8, QStringList &keywords) {
    bool anyAdded = false;

    try {
        QString keywordsTagName(IPTC_KEYWORDS);

        for (Exiv2::IptcData::iterator it = iptcData.begin(); it != iptcData.end(); ++it) {
            QString key = QString::fromLocal8Bit(it->key().c_str());

            if (key == keywordsTagName) {
                QString tag;
                if (isIptcUtf8) {
                    tag = QString::fromUtf8(it->toString().c_str());
                } else {
                    tag = QString::fromLocal8Bit(it->toString().c_str());
                }

                keywords.append(tag);
                anyAdded = true;
            }
        }

        if (keywords.length() == 1 && keywords[0].contains(',')) {
            LOG_DEBUG << "processing legacy saved keywords";
            QString composite = keywords[0];
            keywords.clear();
            keywords += decomposeKeyword(composite);
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyAdded = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyAdded = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyAdded;
}

static bool getIptcDateTime(Exiv2::IptcData &iptcData, bool isIptcUtf8, QDateTime &dateTime) {
    QDate date {};
    QString dateStr;

    if (!getIptcString(iptcData, IPTC_DATE_CREATED, isIptcUtf8, dateStr)) {
        getIptcString(iptcData, IPTC_DIGITAL_DATE_CREATED, isIptcUtf8, dateStr);
    }
    if (!dateStr.isEmpty()) { date = QDate::fromString(dateStr, Qt::ISODate); }

    QTime time {};
    QString timeStr;
    if (!getIptcString(iptcData, IPTC_TIME_CREATED, isIptcUtf8, timeStr)) {
        getIptcString(iptcData, IPTC_DIGITAL_TIME_CREATED, isIptcUtf8, timeStr);
    }
    if (!timeStr.isEmpty()) {
        // NOTE: currently not parsing time offset
        time = QTime::fromString(timeStr, Qt::ISODate);
    }

    if (date.isValid() && time.isValid()) {
        dateTime = QDateTime(date, time);
        return true;
    }

    return false;
}

static QString getExifCommentValue(Exiv2::ExifData &exifData, const QLatin1String &propertyName) {
    QString result;

    Exiv2::ExifKey key(propertyName.data());
    Exiv2::ExifData::iterator it = exifData.findKey(key);
    if (it != exifData.end()) {
        const Exiv2::Exifdatum &exifDatum = *it;
        LOG_DEBUG << "Found Exif comment field for" << propertyName << "size:" << exifDatum.size();

        std::string comment;
        std::string charset;

        comment = exifDatum.toString();

        // libexiv2 will prepend "charset=\"SomeCharset\" " if charset is specified
        // Before conversion to QString, we must know the charset, so we stay with std::string for a while
        if (comment.length() > 8 && comment.substr(0, 8) == "charset=") {
            // the prepended charset specification is followed by a blank
            std::string::size_type pos = comment.find_first_of(' ');

            if (pos != std::string::npos) {
                // extract string between the = and the blank
                charset = comment.substr(8, pos - 8);
                // get the rest of the string after the charset specification
                comment = comment.substr(pos + 1);
            }
        }

        if (charset == "\"Unicode\"") {
            result = QString::fromUtf8(comment.data());
        } else if (charset == "\"Jis\"") {
            QTextCodec *const codec = QTextCodec::codecForName("JIS7");
            result                  = codec->toUnicode(comment.c_str());
        } else if (charset == "\"Ascii\"") {
            result = QString::fromLatin1(comment.c_str());
        } else {
            result = detectEncodingAndDecode(comment);
        }
    }

    return result;
}

static QString ucs2ToString(const std::vector<uint8_t> &ucs2Bytes) {
    const size_t size = ucs2Bytes.size();
    if (size % 2 != 0) { return QString(); }

    std::wstring ucs2String;
    ucs2String.reserve(size / 2);
    for (size_t i = 0; i < size; i += 2) {
        uint16_t ucs2Char = (ucs2Bytes[i + 1] << 8) | ucs2Bytes[i];
        if (ucs2Char == 0) { break; }
        ucs2String.push_back(static_cast<wchar_t>(ucs2Char));
    }

    return QString::fromStdWString(ucs2String);
}

static QString getExifBytesValue(Exiv2::ExifData &exifData, const QLatin1String &propertyName, bool decode = true) {
    QString result;

    Exiv2::ExifKey key(propertyName.data());
    Exiv2::ExifData::iterator it = exifData.findKey(key);
    if (it != exifData.end()) {
        const Exiv2::Exifdatum &exifDatum = *it;
        const Exiv2::Value &value         = exifDatum.value();
        LOG_DEBUG << "Found Exif bytes field for" << propertyName << "size:" << exifDatum.size();

        if (value.ok() && value.size() > 0) {
            std::vector<uint8_t> data;
            data.resize(value.size(), 0);
            long n = value.copy(&data[0], Exiv2::invalidByteOrder);
            if (n > 0) {
                if (decode) {
                    result = ucs2ToString(data);
                } else {
                    result = QString::fromLatin1((const char *)&data[0], strnlen((const char *)&data[0], data.size()));
                }
                if (result.isValidUtf16() && !result.isEmpty()) { return result; }
            }
        } else {
            LOG_DEBUG << "Skipping empty Exif bytes field for" << propertyName;
        }
    }

    return QString();
}

static bool getExifTitle(Exiv2::ExifData &exifData, QString &title) {
    bool foundTitle = false;

    try {
        QString value = getExifBytesValue(exifData, EXIF_XP_TITLE).trimmed();

        if (!value.isEmpty()) {
            LOG_DEBUG << "Processing Exif XP Title";
            title      = value;
            foundTitle = true;
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        foundTitle = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        foundTitle = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return foundTitle;
}

static bool getExifDescription(Exiv2::ExifData &exifData, QString &description) {
    bool foundDesc = false;

    try {
        QString value = getExifCommentValue(exifData, EXIF_DESCRIPTION).trimmed();

        if (value.isEmpty()) { value = getExifCommentValue(exifData, EXIF_USERCOMMENT).trimmed(); }

        if (value.isEmpty()) { value = getExifBytesValue(exifData, EXIF_XP_SUBJECT).trimmed(); }

        if (!value.isEmpty()) {
            description = value;
            foundDesc   = true;
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        foundDesc = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        foundDesc = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return foundDesc;
}

static bool getExifCopyright(Exiv2::ExifData &exifData, QString &copyright) {
    bool foundDesc = false;

    try {
        QString value = getExifCommentValue(exifData, EXIF_COPYRIGHT).trimmed();
        if (!value.isEmpty()) {
            copyright = value;
            foundDesc = true;
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        foundDesc = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        foundDesc = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return foundDesc;
}

static bool getExifKeywords(Exiv2::ExifData &exifData, QStringList &keywords) {
    bool anyAdded = false;

    try {
        QString value = getExifBytesValue(exifData, EXIF_XP_KEYWORDS).trimmed();

        if (!value.isEmpty() && value.contains(',')) {
            LOG_DEBUG << "processing Exif XP keywords";
            keywords.clear();
            keywords += decomposeKeyword(value);
            anyAdded = !keywords.isEmpty();
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        anyAdded = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        anyAdded = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return anyAdded;
}

static bool tryReadExifDateTime(Exiv2::ExifData &exifData, QLatin1String tag, QDateTime &date) {
    bool foundDate = false;
    QString value;

    try {
        value = getExifBytesValue(exifData, tag, false /*decode*/).trimmed();
        if (!value.isEmpty()) {
            QDateTime dt = QDateTime::fromString(value, QLatin1String("yyyy:MM:dd HH:mm:ss"));
            if (dt.isValid()) {
                date      = dt;
                foundDate = true;
            }
        }
    } catch (Exiv2::Error &e) {
        LOG_WARNING << "Exiv2 error:" << e.what();
        foundDate = false;
    } catch (...) {
        LOG_WARNING << "Exception";
        foundDate = false;
#ifdef QT_DEBUG
        throw;
#endif
    }

    return foundDate;
}

static bool getExifDateTime(Exiv2::ExifData &exifData, QDateTime &date) {
    bool result = false;
    result      = result || tryReadExifDateTime(exifData, EXIF_PHOTO_DATETIMEORIGINAL, date);
    result      = result || tryReadExifDateTime(exifData, EXIF_IMAGE_DATETIMEORIGINAL, date);

    // we currently do ignore tz offset as for photos cameras mostly do not have GPS
    // and for non-photos we will prioritize XMP anyways
    return result;
}

static QString
retrieveDescription(Exiv2::XmpData &xmpData, Exiv2::ExifData &exifData, Exiv2::IptcData &iptcData, bool isIptcUtf8) {
    QString description;
    bool success = false;
    success      = getXmpDescription(xmpData, X_DEFAULT, description);
    success      = success || getIptcDescription(iptcData, isIptcUtf8, description);
    success      = success || getExifDescription(exifData, description);
    return description;
}

QString retrieveTitle(Exiv2::XmpData &xmpData, Exiv2::ExifData &exifData, Exiv2::IptcData &iptcData, bool isIptcUtf8) {
    QString title;
    bool success = false;
    success      = getXmpTitle(xmpData, X_DEFAULT, title) && !title.isEmpty();
    success      = success || getIptcTitle(iptcData, isIptcUtf8, title);
    success      = success || getExifTitle(exifData, title);
    Q_UNUSED(exifData);
    return title;
}

static QString
retrieveCopyright(Exiv2::XmpData &xmpData, Exiv2::ExifData &exifData, Exiv2::IptcData &iptcData, bool isIptcUtf8) {
    QString copyright;
    bool success = false;
    success      = getXmpCopyright(xmpData, X_DEFAULT, copyright);
    success      = success || getExifCopyright(exifData, copyright);
    success      = success || getIptcCopyright(iptcData, isIptcUtf8, copyright);
    return copyright;
}

static QString
retrieveHeadline(Exiv2::XmpData &xmpData, Exiv2::ExifData &exifData, Exiv2::IptcData &iptcData, bool isIptcUtf8) {
    Q_UNUSED(xmpData);
    Q_UNUSED(exifData);
    QString headline;
    getIptcHeadline(iptcData, isIptcUtf8, headline);
    return headline;
}

static QStringList
retrieveKeywords(Exiv2::XmpData &xmpData, Exiv2::ExifData &exifData, Exiv2::IptcData &iptcData, bool isIptcUtf8) {
    QStringList keywords;
    bool success = false;
    success      = getXmpKeywords(xmpData, keywords) && !keywords.empty();
    success      = success || getIptcKeywords(iptcData, isIptcUtf8, keywords);
    success      = success || getExifKeywords(exifData, keywords);
    Q_UNUSED(exifData);
    return keywords;
}

static QDateTime
retrieveDateCreated(Exiv2::XmpData &xmpData, Exiv2::ExifData &exifData, Exiv2::IptcData &iptcData, bool isIptcUtf8) {
    QDateTime result;
    // this time around we prioritize EXIF over IPTC as the latter has "weird" date time storage
    bool success = false;
    success      = getXmpDateTime(xmpData, result);
    success      = success || getExifDateTime(exifData, result);
    success      = success || getIptcDateTime(iptcData, isIptcUtf8, result);

    return result;
}

bool readMetadata(const QString &filepath, BasicMetadata &importResult) {
    Exiv2::Image::AutoPtr image;
    try {
#if defined(Q_OS_WIN)
        image = Exiv2::ImageFactory::open(filepath.toStdWString());
#else
        image                       = Exiv2::ImageFactory::open(filepath.toStdString());
#endif
        Q_ASSERT(image.get() != nullptr);
        if (image.get() == nullptr) { return false; }
        image->readMetadata();

        Exiv2::XmpData &xmpData   = image->xmpData();
        Exiv2::ExifData &exifData = image->exifData();
        Exiv2::IptcData &iptcData = image->iptcData();

        QString iptcEncoding = getIptcCharset(iptcData).toUpper();
        bool isIptcUtf8      = (iptcEncoding == QLatin1String("UTF-8")) || (iptcEncoding == QLatin1String("UTF8"));

        importResult.m_Description = retrieveDescription(xmpData, exifData, iptcData, isIptcUtf8);
        importResult.m_Title       = retrieveTitle(xmpData, exifData, iptcData, isIptcUtf8);
        importResult.m_Copyright   = retrieveCopyright(xmpData, exifData, iptcData, isIptcUtf8);
        importResult.m_Headline    = retrieveHeadline(xmpData, exifData, iptcData, isIptcUtf8);
        importResult.m_Keywords    = retrieveKeywords(xmpData, exifData, iptcData, isIptcUtf8);
        importResult.m_CreatedAt   = retrieveDateCreated(xmpData, exifData, iptcData, isIptcUtf8);

        if ((importResult.m_Title.isEmpty() && importResult.m_Description.isEmpty()) &&
            filepath.endsWith(".png", Qt::CaseInsensitive)) {
            auto metadata = readPNGMetadata(filepath);
            for (auto &pair: metadata) {
                if (pair.first == "Description") {
                    if (importResult.m_Description.isEmpty()) {
                        importResult.m_Description = QString::fromStdString(pair.second);
                    }
                } else if (pair.first == "Title") {
                    if (importResult.m_Title.isEmpty()) { importResult.m_Title = QString::fromStdString(pair.second); }
                } else if (pair.first == "Copyright") {
                    if (importResult.m_Copyright.isEmpty()) {
                        importResult.m_Copyright = QString::fromStdString(pair.second);
                    }
                }
            }
        }

        return true;
    } catch (const Exiv2::Error &err) {
        LOG_WARNING << "Couldn't read metadata:" << err.what();
        return false;
    }
#ifdef EXV_UNICODE_PATH
    catch (const Exiv2::WError &err) {
        LOG_WARNING << "Couldn't read metadata:" << err.what();
        return false;
    }
#endif
}

bool createThumbnail(const QString &filepath, const QString &outPathTemplate, QString &extension) {
    LOG_INFO << "Creating thumbnail for:" << PII(filepath);
    Exiv2::Image::AutoPtr image;
    try {
#if defined(Q_OS_WIN)
        image = Exiv2::ImageFactory::open(filepath.toStdWString());
#else
        image                       = Exiv2::ImageFactory::open(filepath.toStdString());
#endif
        Q_ASSERT(image.get() != nullptr);
        image->readMetadata();

        Exiv2::PreviewManager pm(*image);
        Exiv2::PreviewPropertiesList list = pm.getPreviewProperties();
        if (list.empty()) { return false; }

        uint32_t maxSize  = 0;
        size_t index      = 0;
        bool found        = false;
        const size_t size = list.size();

        for (size_t i = 0; i < size; i++) {
            auto &previewProperty = list.at(i);
            if ((previewProperty.mimeType_ == "image/jpeg") || (previewProperty.mimeType_ == "image/tiff") ||
                (previewProperty.mimeType_ == "image/png")) {
                if (previewProperty.size_ > maxSize) {
                    maxSize = previewProperty.size_;
                    index   = i;
                    found   = true;
                }
            }
        }

        if (found) {
            auto const &previewProperty = list.at(index);
            auto preview                = pm.getPreviewImage(previewProperty);
            size_t dummy                = 0;
#if defined(Q_OS_WIN)
            dummy = preview.writeFile(outPathTemplate.toStdWString());
#else
            dummy = preview.writeFile(outPathTemplate.toStdString());
#endif
            extension = QString::fromStdString(preview.extension());
            Q_UNUSED(dummy);
        }

        LOG_DEBUG << "Finished creating thumbnail" << PII(filepath);

        return found;
    } catch (const Exiv2::Error &err) {
        LOG_WARNING << "Couldn't create preview:" << err.what();
        return false;
    }
#ifdef EXV_UNICODE_PATH
    catch (const Exiv2::WError &err) {
        LOG_WARNING << "Couldn't create preview:" << err.what();
        return false;
    }
#endif
}

static void writeExifData(Exiv2::ExifData &exifData, const BasicMetadata &metadata) {
    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseDescription)) {
        exifData[EXIF_DESCRIPTION.data()] = metadata.m_Description.toUtf8().toStdString();
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseCopyright)) {
        exifData[EXIF_COPYRIGHT.data()] = metadata.m_Copyright.toUtf8().toStdString();
    }
}

static void writeIptcData(Exiv2::IptcData &iptcData, const BasicMetadata &metadata) {
    if (iptcData.findKey(Exiv2::IptcKey(IPTC_CHARSET.data())) == iptcData.end()) {
        iptcData[IPTC_CHARSET.data()] = IPTC_CHARSET_UTF8.data();
    }

    QString iptcEncoding = getIptcCharset(iptcData).toUpper();

    QTextCodec *codec = QTextCodec::codecForName(iptcEncoding.toStdString().c_str());
    auto convert      = [codec](const QString &str) {
        if (codec) { return codec->fromUnicode(str).toStdString(); }
        return str.toLocal8Bit().toStdString();
    };

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseTitle)) {
        iptcData[IPTC_TITLE.data()] = convert(metadata.m_Title);
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseDescription)) {
        iptcData[IPTC_DESCRIPTION.data()] = convert(metadata.m_Description);
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseCopyright)) {
        iptcData[IPTC_COPYRIGHT.data()] = convert(metadata.m_Copyright.left(IPTC_COPYRIGHT_MAX_LENGTH));
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseHeadline)) {
        iptcData[IPTC_HEADLINE.data()] = convert(metadata.m_Headline.left(IPTC_HEADLINE_MAX_LENGTH));
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseKeywords)) {
        Exiv2::IptcData newIptcData;
        QString keywordsTagName(IPTC_KEYWORDS);

        for (const auto &data: iptcData) {
            QString key = QString::fromLocal8Bit(data.key().c_str());
            if (key != keywordsTagName) { newIptcData.add(data); }
        }

        for (const QString &keyword: metadata.m_Keywords) {
            Exiv2::StringValue value(convert(keyword));
            newIptcData.add(Exiv2::IptcKey(IPTC_KEYWORDS.data()), &value);
        }

        std::swap(iptcData, newIptcData);
    }
}

static void writeXmpData(Exiv2::XmpData &xmpData, const BasicMetadata &metadata) {
    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseTitle)) {
        xmpData[XMP_TITLE.data()] = metadata.m_Title.toUtf8().toStdString();
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseDescription)) {
        xmpData[XMP_DESCRIPTION.data()] = metadata.m_Description.toUtf8().toStdString();
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseCopyright)) {
        xmpData[XMP_COPYRIGHT.data()] = metadata.m_Copyright.toUtf8().toStdString();
    }

    if (Common::HasFlag(metadata.m_UseFlags, Common::MetadataFlags::UseKeywords)) {
        Exiv2::XmpKey xmpKeywords(XMP_KEYWORDS.data());
        auto it = xmpData.findKey(xmpKeywords);
        if (it != xmpData.end()) { xmpData.erase(xmpData.findKey(xmpKeywords)); }

        Exiv2::XmpTextValue xmpValue;
        for (const QString &keyword: qAsConst(metadata.m_Keywords)) {
            xmpValue.read(keyword.toUtf8().toStdString());
            xmpData[XMP_KEYWORDS.data()] = xmpValue.value_;
        }
    }
}

bool writeMetadata(const QString &filepath, const BasicMetadata &metadata) {
    LOG_DEBUG << "Writing metadata directly to" << PII(filepath);
    try {
#if defined(Q_OS_WIN)
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filepath.toStdWString());
#else
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filepath.toStdString());
#endif
        Q_ASSERT(image.get() != nullptr);
        image->readMetadata();

        writeExifData(image->exifData(), metadata);
        writeIptcData(image->iptcData(), metadata);
        writeXmpData(image->xmpData(), metadata);

        image->writeMetadata();
        return true;
    } catch (const Exiv2::Error &err) {
        LOG_WARNING << "Couldn't write metadata:" << err.what();
        return false;
    }
#ifdef EXV_UNICODE_PATH
    catch (const Exiv2::WError &err) {
        LOG_WARNING << "Couldn't write metadata:" << err.what();
        return false;
    }
#endif
}

bool wipeMetadata(const QString &filepath) {
    try {
#if defined(Q_OS_WIN)
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filepath.toStdWString());
#else
        Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filepath.toStdString());
#endif
        Q_ASSERT(image.get() != nullptr);
        image->clearMetadata();
        image->writeMetadata();
        return true;
    } catch (const Exiv2::Error &err) {
        LOG_WARNING << "Couldn't wipe metadata:" << err.what();
        return false;
    }
#ifdef EXV_UNICODE_PATH
    catch (const Exiv2::WError &err) {
        LOG_WARNING << "Couldn't wipe metadata:" << err.what();
        return false;
    }
#endif
}
