/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "filehelpers.h"

#include <QFile>
#include <QIODevice>
#include <QString>
#include <QThread>

#include "Common/logging.h"

namespace Helpers {
    const int REMOVE_FILE_RETRY_INTERVAL = 200;
    const int PROXY_FILE_INTERVAL        = 200;
    const int PROXY_BUFFER_SIZE          = 4096;
    const int RENAME_FILE_INTERVAL       = 100;

    bool removeFile(const QString &fullpath, int retries) {
        LOG_DEBUG << "Attempting to remove file" << PII(fullpath);
        bool removed = false;

        int attempt = 0;
        do {
            QFile file(fullpath);
            if (!file.exists()) {
                removed = true;
                break;
            }

            if (file.remove()) {
                removed = true;
                break;
            }
            LOG_WARNING << "Removing" << PII(fullpath) << "failed, error:" << file.errorString();
            QThread::usleep(REMOVE_FILE_RETRY_INTERVAL);
        } while (attempt++ < retries);

        return removed;
    }

    bool copyToProxy(const QString &filepath, qint64 maxSize, QFile &proxy) {
        const int maxAttempts = 5;
        bool written          = false;
        for (int i = 0; !written && (i < maxAttempts); i++) {
            QFile file(filepath);

            if (!file.exists()) {
                LOG_WARNING << "File" << PII(filepath) << "does not exist";
                break;
            }

            const qint64 size = file.size();
            if (file.size() >= maxSize) {
                LOG_WARNING << PII(filepath) << "size" << size << "is larger than" << maxSize;
                break;
            }

            if (!file.open(QIODevice::ReadOnly)) {
                LOG_WARNING << "Failed to open" << PII(filepath) << ". Error:" << file.errorString();
                break;
            }

            char block[PROXY_BUFFER_SIZE];
            qint64 bytes  = 0;
            bool anyError = false;
            while ((bytes = file.read(block, sizeof(block))) > 0) {
                if (bytes != proxy.write(block, bytes)) {
                    LOG_WARNING << "Failed to write data to the proxy";
                    anyError = true;
                    break;
                }
            }

            if (!anyError) {
                written = true;
                break;
            }

            QThread::usleep(PROXY_FILE_INTERVAL);
        }

        return written;
    }

    bool renameFile(const QString &from, const QString &to) {
        const int maxAttempts = 5;
        bool renamed          = false;
        for (int attempt = 0; !renamed && (attempt < maxAttempts); attempt++) {
            QFile file(from);
            renamed = file.rename(to);
            QThread::usleep(RENAME_FILE_INTERVAL);
            if (!renamed) { LOG_WARNING << "Rename attempt failed. Error:" << file.errorString(); }
        }
        if (!renamed) { LOG_WARNING << "Failed to rename" << PII(from) << "->" << PII(to) << REPORT_ME; }
        return renamed;
    }
}  // namespace Helpers
