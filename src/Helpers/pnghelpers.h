#ifndef PNGHELPERS_H
#define PNGHELPERS_H

#include <string>
#include <utility>
#include <vector>

class QString;

std::vector<std::pair<std::string, std::string>> readPNGMetadata(const QString &filepath);

#endif // PNGHELPERS_H
