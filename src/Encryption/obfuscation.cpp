/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "obfuscation.h"

#include <QChar>
#include <QCharRef>
#include <QtGlobal>

namespace Encryption {
    QString rot13plus(const QString &line) {
        QString result = line;

        const int size = line.size();
        for (int i = 0; i < size; i++) {
            QChar c  = result[i];
            ushort u = c.unicode();

            if (c >= QChar('!') && c <= QChar('~')) { u = '!' + (u - '!' + 47) % 94; }

            result[i] = QChar(u);
        }

        return result;
    }
}  // namespace Encryption
