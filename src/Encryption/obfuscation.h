/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef OBFUSCATION_H
#define OBFUSCATION_H

#include <QString>

namespace Encryption {
    QString rot13plus(const QString &line);
}

#endif  // OBFUSCATION_H
