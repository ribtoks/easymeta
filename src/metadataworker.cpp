/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "metadataworker.h"

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QTemporaryFile>
#include <QtConcurrent>

#include "Helpers/constants.h"
#include "Helpers/exiv2iohelpers.h"
#include "Helpers/filehelpers.h"

const int REMOVE_BACKUP_RETRIES         = 4;
const int PARALLEL_PROCESSING_THRESHOLD = 50;
const qint64 MAX_PROXY_FILESIZE         = 100 * 1024 * 1024;

static void responseAvailable(const QJsonDocument &response) {
    const QByteArray data    = response.toJson(QJsonDocument::Compact);
    const std::string result = data.toStdString();
    std::cout << result << std::endl;
    LOG_DEBUG << "Wrote:" << result.size() << "bytes";
#ifdef QT_DEBUG
    LOG_DEBUG << "Response:" << QString::fromLocal8Bit(data);
#endif
}

static void queueIsEmpty() {
    // std::endl should flush as well, but just to be on the safe side
    std::cout << std::flush;
}

static void setMetadataFromJson(const QJsonObject &object, BasicMetadata &metadata) {
    if (object.contains(TITLE_KEY)) {
        metadata.m_Title = object[TITLE_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseTitle);
    }

    if (object.contains(DESCRIPTION_KEY)) {
        metadata.m_Description = object[DESCRIPTION_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseDescription);
    }

    if (object.contains(COPYRIGHT_KEY)) {
        metadata.m_Copyright = object[COPYRIGHT_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseCopyright);
    }

    if (object.contains(HEADLINE_KEY)) {
        metadata.m_Headline = object[HEADLINE_KEY].toString();
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseHeadline);
    }

    if (object.contains(KEYWORDS_KEY)) {
        QJsonArray keywordsArray = object[KEYWORDS_KEY].toArray();
        metadata.m_Keywords.reserve(keywordsArray.size());

        for (const auto &keyword: qAsConst(keywordsArray)) {
            if (keyword.isString()) { metadata.m_Keywords.append(keyword.toString()); }
        }

        // we might actually want to write empty keywords
        // if (!metadata.m_Keywords.isEmpty()) {
        Common::SetFlag(metadata.m_UseFlags, Common::MetadataFlags::UseKeywords);
    }
}

static bool validateFileRequest(const QString &filepath, QJsonObject &fileResponse) {
    if (filepath.isEmpty()) {
        LOG_WARNING << "Encountered an empty file path";
        fileResponse[ERROR_KEY] = "File name is missing or empty";
        return false;
    }

    if (!QFileInfo::exists(filepath)) {
        LOG_WARNING << "File doesn't exist:" << PII(filepath);
        fileResponse[ERROR_KEY] = "File doesn't exist";
        return false;
    }

    return true;
}

static void removeBackup(const QString &filepath) {
    const QString backupPath = filepath + "_original";
    if (!Helpers::removeFile(backupPath, REMOVE_BACKUP_RETRIES)) {
        LOG_WARNING << "Failed to remove previous backup copy of" << PII(filepath);
    }
}

static bool backupFile(const QString &filepath, QJsonObject &fileResponse) {
    const QString backupPath = filepath + "_original";

    if (!Helpers::removeFile(backupPath, REMOVE_BACKUP_RETRIES)) {
        LOG_WARNING << "Failed to remove previous backup copy of" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't remove previous backup copy";
        return false;
    }

    QFile file(filepath);
    if (!file.copy(filepath, backupPath)) {
        LOG_WARNING << "Failed to make a backup copy of" << PII(filepath) << ". Error:" << file.errorString();
        fileResponse[ERROR_KEY] = "Couldn't make a backup copy";
        return false;
    }

    return true;
}

static void restoreFile(const QString &filepath, QJsonObject &fileResponse) {
    const QString backupPath = filepath + "_original";

    QFile backup(backupPath);
    if (!backup.exists()) {
        LOG_WARNING << "Failed to find backup copy of" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't find backup copy";
        return;
    }

    if (!QFile::remove(filepath)) {
        LOG_WARNING << "Failed to remove" << PII(filepath);
        fileResponse[ERROR_KEY] = "Couldn't remove the file";
        return;
    }

    if (!backup.rename(filepath)) {
        LOG_WARNING << "Failed to rename backup copy of" << PII(filepath);
        if (!backup.copy(filepath)) {
            LOG_WARNING << "Failed to copy backup file" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't move nor copy backup file";
        } else {
            LOG_INFO << "Copied file instead of moving it" << PII(filepath);
        }
    }
}

std::shared_ptr<QJsonDocument> MetadataWorker::processWorkItem(WorkItem &workItem) {
    const QString id = workItem.m_Item->value(ID_KEY).toString();
    LOG_DEBUG << "Processing ID:" << id;

    if (id.isEmpty()) {
        LOG_WARNING << "Empty request ID";
        return {};
    }

    auto response              = std::make_shared<QJsonDocument>();
    const QJsonObject &request = *workItem.m_Item;
    QString type               = workItem.m_Item->value(TYPE_KEY).toString();

    if (type == READ_TYPE) {
        handleRead(request, *response);
    } else if (type == WRITE_TYPE) {
        handleWrite(request, *response);
    } else if (type == WIPE_TYPE) {
        handleWipe(request, *response);
    } else if (type == RESTORE_TYPE) {
        handleRestore(request, *response);
    } else if (type == THUMBNAIL_TYPE) {
        handleThumbnail(request, *response);
    } else {
        LOG_WARNING << "Unknown request type:" << type;
        QJsonObject reply;
        reply[ID_KEY]    = request[ID_KEY];
        reply[ERROR_KEY] = "Unknown request type: " + type;
        response->setObject(reply);
    }

    return response;
}

void MetadataWorker::onResultsAvailable(std::vector<WorkResult> &results) {
    LOG_DEBUG << results.size();

    for (const WorkResult &result: results) {
        QJsonDocument &response = *result.m_Result;
        /*emit*/ responseAvailable(response);
    }

    // std::endl should flush as well, but just to be on the safe side
    std::cout << std::flush;
}

void MetadataWorker::onQueueIsEmpty() {
    LOG_DEBUG << "#";
    /*emit*/ queueIsEmpty();
}

static QJsonArray forEachArrayItem(const QJsonArray &array,
                                   const std::function<QJsonObject(const QJsonValue &)> &transform) {
    if (array.size() >= PARALLEL_PROCESSING_THRESHOLD) {
        return QtConcurrent::blockingMappedReduced(array, transform, &QJsonArray::append);
    }

    QJsonArray result;
    for (const QJsonValue &item: array) {
        result.append(transform(item));
    }
    return result;
}

static QJsonObject doReadMetadata(const QJsonValue &value) {
    const QString filepath = value.toString();
    LOG_INFO << "Reading:" << PII(filepath);

    BasicMetadata metadata;
    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        if (!readMetadata(filepath, metadata)) {
            LOG_WARNING << "Failed to read metadata of" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't read metadata";
            break;
        }

        fileResponse.insert(TITLE_KEY, metadata.m_Title);
        fileResponse.insert(DESCRIPTION_KEY, metadata.m_Description);
        fileResponse.insert(COPYRIGHT_KEY, metadata.m_Copyright);
        fileResponse.insert(HEADLINE_KEY, metadata.m_Headline);
        QJsonArray keywords = keywords.fromStringList(metadata.m_Keywords);
        fileResponse.insert(KEYWORDS_KEY, keywords);
        if (metadata.m_CreatedAt.isValid()) {
            fileResponse.insert(CREATED_AT_KEY, metadata.m_CreatedAt.toString(Qt::ISODate));
        }

        LOG_DEBUG << "Finished reading" << PII(filepath);
    } while (false);

    return fileResponse;
}

static QJsonObject doCreateThumbnail(const QJsonValue &value, const QString &dir) {
    QJsonObject file       = value.toObject();
    const QString filepath = file[SOURCE_FILE_KEY].toString();

    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        const QString outFilename = file[OUT_FILENAME_KEY].toString();
        if (outFilename.isEmpty()) {
            LOG_WARNING << "Encountered an empty out filename";
            fileResponse[ERROR_KEY] = "Out filename is missing or empty";
            break;
        }

        const QString outTemplate = QDir::cleanPath(dir + '/' + outFilename);

        QString extension;
        if (!createThumbnail(filepath, outTemplate, extension)) { break; }

        fileResponse[EXTENSION_KEY] = extension;
    } while (false);

    return fileResponse;
}

void MetadataWorker::handleRead(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    QJsonArray files      = request[FILES_KEY].toArray();
    QJsonArray filesArray = forEachArrayItem(files, doReadMetadata);

    QJsonObject reply;
    reply[ID_KEY]    = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

static bool writeProxyMetadata(const QString &filepath, const BasicMetadata &metadata) {
    LOG_DEBUG << "Writing metadata via proxy to" << PII(filepath);
    bool success = false;
    QTemporaryFile tempFile;

    do {
        if (!tempFile.open()) {
            LOG_WARNING << "Failed to open temporary file";
            break;
        }

        if (!Helpers::copyToProxy(filepath, MAX_PROXY_FILESIZE, tempFile)) {
            LOG_WARNING << "Failed to copy file to proxy";
            break;
        }

        const QString proxyFilepath = tempFile.fileName();
        tempFile.close();

        if (!writeMetadata(proxyFilepath, metadata)) {
            LOG_WARNING << "Failed to write metadata to proxy";
            break;
        }

        LOG_DEBUG << "Writing to temporary proxy succeeded. Renaming files...";
        const QString swapFilepath = filepath + ".xpks.tmp";
        if (!Helpers::removeFile(swapFilepath, 5)) {
            LOG_WARNING << "Cannot remove temp swap file" << PII(swapFilepath);
            break;
        }

        // although it would have been easier to rename the original file,
        // we want to attempt to rename the proxy first, since in this codepath
        // are not sure that IO operations work fine there
        if (tempFile.copy(swapFilepath)) {
            if (Helpers::removeFile(filepath, 5)) { success = Helpers::renameFile(swapFilepath, filepath); }
        } else {
            LOG_WARNING << "Failed to copy temporary proxy to swap file";
        }
    } while (false);

    return success;
}

static bool canBeProxied(const QString &filepath) {
    QFile file(filepath);
    if (!file.open(QIODevice::ReadOnly)) { return false; }

    const int MEGABYTE = 1024 * 1024;
    if (file.size() > 100 * MEGABYTE) { return false; }

    return true;
}

static QJsonObject doWriteMetadata(const QJsonValue &value, bool withDirect, bool withBackups, bool withProxy) {
    QJsonObject file       = value.toObject();
    const QString filepath = file[SOURCE_FILE_KEY].toString();
    LOG_INFO << "Writing:" << PII(filepath) << "direct:" << withDirect << "proxy:" << withProxy;

    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        BasicMetadata metadata;
        setMetadataFromJson(file, metadata);

        if (withBackups && !backupFile(filepath, fileResponse)) { break; }

        if (withDirect) {
            if (writeMetadata(filepath, metadata)) { break; }

            fileResponse[ERROR_KEY] = "Couldn't update metadata";
            LOG_WARNING << "Failed to update metadata of" << PII(filepath);
        }

        if (withProxy && canBeProxied(filepath)) {
            QJsonObject proxyResponse;
            if (!backupFile(filepath, proxyResponse)) {
                LOG_WARNING << "Skipping writing via proxy without backup";
                break;
            }

            if (writeProxyMetadata(filepath, metadata)) {
                fileResponse.remove(ERROR_KEY);
                if (!withBackups) { removeBackup(filepath); }
                LOG_DEBUG << "Wrote metadata via proxy";
                break;
            }

            restoreFile(filepath, proxyResponse);
        }

        LOG_DEBUG << "Finished writing" << PII(filepath);
    } while (false);

    return fileResponse;
}

void MetadataWorker::handleWrite(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

#ifdef QT_DEBUG
    const bool withDirect = request[WITH_DIRECT_KEY].toBool(true /*default*/);
#else
    const bool withDirect = true;
#endif
    const bool withBackups = request[BACKUPS_KEY].toBool();
    const bool withProxy   = request[WITH_PROXY_KEY].toBool();
    QJsonArray files       = request[FILES_KEY].toArray();

    auto transform = [withDirect, withBackups, withProxy](const QJsonValue &value) {
        return doWriteMetadata(value, withDirect, withBackups, withProxy);
    };
    QJsonArray filesArray = forEachArrayItem(files, transform);

    QJsonObject reply;
    reply[ID_KEY]    = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

static QJsonObject doWipeMetadata(const QJsonValue &value, bool withBackups) {
    const QString filepath = value.toString();

    QJsonObject fileResponse;
    fileResponse.insert(SOURCE_FILE_KEY, filepath);

    do {
        if (!validateFileRequest(filepath, fileResponse)) { break; }

        if (withBackups && !backupFile(filepath, fileResponse)) { break; }

        if (!wipeMetadata(filepath)) {
            LOG_WARNING << "Failed to wipe metadata of" << PII(filepath);
            fileResponse[ERROR_KEY] = "Couldn't wipe metadata";
        }

        LOG_DEBUG << "Finished wiping" << PII(filepath);
    } while (false);

    return fileResponse;
}

void MetadataWorker::handleWipe(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    const bool withBackups = request[BACKUPS_KEY].toBool();
    QJsonArray files       = request[FILES_KEY].toArray();

    auto transform        = [withBackups](const QJsonValue &value) { return doWipeMetadata(value, withBackups); };
    QJsonArray filesArray = forEachArrayItem(files, transform);

    QJsonObject reply;
    reply[ID_KEY]    = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

void MetadataWorker::handleRestore(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    QJsonArray files = request[FILES_KEY].toArray();
    QJsonArray filesArray;

    const int size = files.size();
    for (int i = 0; i < size; ++i) {
        QString filepath = files.at(i).toString();

        QJsonObject fileResponse;
        fileResponse.insert(SOURCE_FILE_KEY, filepath);

        do {
            if (!validateFileRequest(filepath, fileResponse)) { break; }

            restoreFile(filepath, fileResponse);
        } while (false);

        filesArray.append(fileResponse);
    }

    QJsonObject reply;
    reply[ID_KEY]    = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}

void MetadataWorker::handleThumbnail(const QJsonObject &request, QJsonDocument &response) {
    LOG_DEBUG << "#";

    QJsonArray files     = request[FILES_KEY].toArray();
    const QString outDir = request[OUT_DIR_KEY].toString();

    auto handler          = [&outDir](const QJsonValue &value) { return doCreateThumbnail(value, outDir); };
    QJsonArray filesArray = forEachArrayItem(files, handler);

    QJsonObject reply;
    reply[ID_KEY]    = request[ID_KEY];
    reply[FILES_KEY] = filesArray;
    response.setObject(reply);
}
