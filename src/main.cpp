/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include <cstdint>
#include <iostream>

#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QThread>

#include "Helpers/exiv2iohelpers.h"
#include "Helpers/logger.h"
#include "Helpers/loggingworker.h"
#include "Helpers/signalhandler.h"
#include "metadataworker.h"
#include "version.h"

#include <libpng/png.h>

#include <chillout/src/chillout/chillout.h>

static Helpers::LoggingWorker *startLogger() {
    Helpers::LoggingWorker *loggingWorker = nullptr;

    Helpers::Logger &logger = Helpers::Logger::getInstance();
    QString logDirPath      = QString::fromUtf8(qgetenv("LOGS_DIR"));
    logger.setMemoryOnly(logDirPath.isEmpty());

    if (!logDirPath.isEmpty()) {
        QDir logsDir(logDirPath);
        if (logsDir.exists()) {
            QString time        = QDateTime::currentDateTimeUtc().toString("ddMMyyyy-hhmmss-zzz");
            QString logFilename = QString("easymeta-%1.log").arg(time);
            QString logFilePath = logsDir.filePath(logFilename);
            logger.setLogFilePath(logFilePath);

            loggingWorker          = new Helpers::LoggingWorker();
            QThread *loggingThread = new QThread();
            loggingWorker->moveToThread(loggingThread);

            QObject::connect(loggingThread, &QThread::started, loggingWorker, &Helpers::LoggingWorker::process);
            QObject::connect(loggingWorker, &Helpers::LoggingWorker::stopped, loggingThread, &QThread::quit);

            QObject::connect(loggingWorker, &Helpers::LoggingWorker::stopped, loggingWorker,
                             &Helpers::LoggingWorker::deleteLater);
            QObject::connect(loggingThread, &QThread::finished, loggingThread, &QThread::deleteLater);

            loggingThread->start(QThread::LowPriority);
        } else {
            logger.setMemoryOnly(true);
        }
    }

    return loggingWorker;
}

void initCrashRecovery() {
    auto &chillout = Debug::Chillout::getInstance();
    // crash dir is not used since we do not create a crash dump in crash callback
#ifdef Q_OS_WIN
    chillout.init(L"easymeta", L"C:\temp");
#else
    chillout.init("easymeta", "/tmp");
#endif
    Helpers::Logger &logger = Helpers::Logger::getInstance();

    chillout.setBacktraceCallback([&logger](const char *const stackTrace) { logger.emergencyLog(stackTrace); });

    chillout.setCrashCallback([&logger, &chillout]() {
        chillout.backtrace();
        logger.emergencyFlush();
    });
}

const int PROCESS_EVENTS_MILLIS = 250;

struct ForceToBeLinked
{
    ForceToBeLinked(const void *p) { LOG_DEBUG << uintptr_t(p); }
};

ForceToBeLinked forceLibPng((void *)png_create_info_struct);

class Application: public SignalHandler
{
private:
    MetadataWorker m_Worker;

public:
    int mainHandler(int argc, char *argv[]) {
        QCoreApplication app(argc, argv);

        QStringList args = app.arguments();
        if (args.contains("-v") || args.contains("--version")) {
            std::cout << "Easy meta: " << Version::GIT_COMMIT_SHA1 << '\n';
            return 0;
        }

        Helpers::LoggingWorker *loggingWorker = startLogger();

        Exiv2InitHelper exiv2Init;
        Q_UNUSED(exiv2Init);

        LOG_DEBUG << "Starting worker...";
        m_Worker.start();
        // don't send requests until the worker is ready to serve them
        m_Worker.waitIdle();

#ifdef QT_DEBUG
        if (args.size() > 1) {
            QJsonParseError error {};
            auto document = QJsonDocument::fromJson(args[1].toLocal8Bit(), &error);
            if (document.isObject()) {
                m_Worker.submitItem(std::make_shared<QJsonObject>(document.object()));
            } else {
                LOG_DEBUG << "Skipping invalid request:" << error.errorString();
            }
        }
#endif

        LOG_DEBUG << "Starting reading from STDIN";
        for (std::string request; std::getline(std::cin, request);) {
            if (request == "quit") {
                LOG_DEBUG << "Received quit command";
                break;
            }
            LOG_DEBUG << "Read" << request.size() << "bytes from STDIN";

            auto document = QJsonDocument::fromJson(QByteArray::fromStdString(request));
            if (document.isObject()) {
                m_Worker.submitItem(std::make_shared<QJsonObject>(document.object()));
            } else {
                LOG_DEBUG << "Skipping invalid request";
            }
        }

        LOG_DEBUG << "Finished reading from STDIN";
        LOG_INFO << "Worker pending requests:" << m_Worker.retrievePendingCount();

        // give some time to process whatever is in the queue
        m_Worker.stopWorking(false /*immediately*/);

        LOG_DEBUG << "Waiting for worker to finish";
        m_Worker.wait();

        if (loggingWorker != nullptr) { loggingWorker->cancel(); }

        // make sure to flush everything we could have written in the worker
        std::cout << std::flush;

        // finalize everything possible
        QThread::msleep(PROCESS_EVENTS_MILLIS);
        QCoreApplication::processEvents(QEventLoop::AllEvents, PROCESS_EVENTS_MILLIS);
        return 0;
    }

    virtual bool handleSignal(int signal) override {
        LOG_INFO << signal;

        if (m_Worker.isRunning()) {
            m_Worker.cancelPendingJobs();
            // don't propagate this signal further
            return true;
        }

        // Let the signal propagate as though we had not been there
        return false;
    }
};

int main(int argc, char *argv[]) {
    initCrashRecovery();

    Application app;
    return app.mainHandler(argc, argv);
}
