/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef THUMBNAILTESTS_H
#define THUMBNAILTESTS_H

#include <QObject>
#include <QProcess>
#include <QTemporaryDir>
#include <QtTest>

class ThumbnailTests : public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();

    void vectorThumbnailTest();

private:
    QProcess m_EasyMeta;
    QTemporaryDir m_Dir;
};

#endif // THUMBNAILTESTS_H
