/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef TESTHELPERS_H
#define TESTHELPERS_H

#include <QJsonArray>
#include <QJsonDocument>
#include <QProcess>
#include <QString>
#include <QStringList>

constexpr int TIMEOUT_MS = 1000;

QString asString(QStringList list);
QString asString(const QJsonArray &array);
QString findFullPathForTests(const QString &prefix);
QString setupFilePathForTest(const QString &prefix);
void cleanupSessionDir();
bool sendSimpleRequest(QProcess &easyMeta, const QString &type, const QStringList &files, bool withBackups = true);
void receiveResponse(QProcess &easyMeta, QJsonObject &response);
void readWithExiftool(const QString &path, QJsonDocument &document);
void checkFileErrorResponse(QProcess &easyMeta, const QString &error);

#endif  // TESTHELPERS_H
