/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef GENERICTESTS_H
#define GENERICTESTS_H

#include <QObject>
#include <QProcess>
#include <QtTest>

class GenericTests: public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();

    void missingIdTest();
    void missingTypeTest();
    void unknownTypeTest();

private:
    QProcess m_EasyMeta;
};

#endif  // GENERICTESTS_H
