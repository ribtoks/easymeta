/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef WRITETESTS_H
#define WRITETESTS_H

#include <QObject>
#include <QProcess>
#include <QtTest>

class WriteTests: public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();

    void badFileEntryTest();
    void missingFileTest();
    void unsupportedFileTest();
    void jpgFileTest();
    void pngFileTest();
    void rawFileTest();
    void multipleFilesTest();
    void asciiRoundTripTest();
    void unicodeRoundTripTest();

private:
    QProcess m_EasyMeta;
};

#endif  // WRITETESTS_H
