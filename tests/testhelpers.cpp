/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "testhelpers.h"

#include <chrono>
#include <thread>

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include <QtTest>

#include "Common/logging.h"
#include "Helpers/constants.h"

#define STRINGIZE_(x) #x
#define STRINGIZE(x)  STRINGIZE_(x)

#define TEST_PREFIX STRINGIZE(EASYMETA_TESTS_ROOT)

const std::chrono::milliseconds SLEEP_PERIOD(500);
const int WAIT_PERIOD_COUNT = 10;

QString asString(QStringList list) {
    list.sort();
    return list.join(";");
}

QString asString(const QJsonArray &array) {
    QStringList list;
    for (const QJsonValue &value: qAsConst(array)) {
        list << value.toString();
    }
    return asString(list);
}

static bool tryFindFullPathForTests(const QString &prefix, QString &path) {
    QFileInfo fi(prefix);
    int tries = 6;
    QStringList parents;
    while (tries--) {
        if (!fi.exists()) {
            parents.append("..");
            fi.setFile(parents.join('/') + "/" + prefix);
        } else {
            path = fi.absoluteFilePath();
            return true;
        }
    }

    return false;
}

QString findFullPathForTests(const QString &prefix) {
    QString foundPath;
    if (!tryFindFullPathForTests(TEST_PREFIX "/" + prefix, foundPath) && !tryFindFullPathForTests(prefix, foundPath)) {
        foundPath = QFileInfo(prefix).absoluteFilePath();
    }
    return foundPath;
}

static void ensureDirectoryExistsForFile(const QString &filepath) {
    QFileInfo fi(filepath);

    if (fi.exists()) { return; }

    QDir filesDir = fi.absoluteDir();
    if (!filesDir.exists()) {
        if (!filesDir.mkpath(".")) { qWarning() << "Failed to create a path:" << filesDir.path(); }
    }
}

static bool copyFile(const QString &from, const QString &to) {
    bool success = false;

    QFile destination(to);
    if (destination.exists()) {
        LOG_WARNING << "Destination file exists:" << PII(to);
        if (!destination.remove()) {
            LOG_WARNING << "Failed to remove destination:" << PII(to);
            return success;
        }
    }

    QFile source(from);
    if (source.exists()) { success = source.copy(to); }

    return success;
}

QString setupFilePathForTest(const QString &prefix) {
    QString fullPath;
    if (tryFindFullPathForTests(TEST_PREFIX "/" + prefix, fullPath) || tryFindFullPathForTests(prefix, fullPath)) {
        qInfo() << "Found artwork at" << fullPath;
        QString sessionPath = QDir::cleanPath(QDir::currentPath() + "/session/" + prefix);
        ensureDirectoryExistsForFile(sessionPath);
        if (copyFile(fullPath, sessionPath)) {
            qInfo() << "Copied artwork to" << sessionPath;
            return sessionPath;
        } else {
            return fullPath;
        }
    }

    return prefix;
}

void cleanupSessionDir() {
    QDir sessionDir("session");
    if (!sessionDir.removeRecursively()) { qWarning() << "Failed to remove session directory"; }
}

bool sendSimpleRequest(QProcess &easyMeta, const QString &type, const QStringList &files, bool withBackups) {
    if (easyMeta.state() != QProcess::Running) { return false; }
    QJsonObject request;
    request[ID_KEY]      = QLatin1String(QTest::currentTestFunction());
    request[TYPE_KEY]    = type;
    request[BACKUPS_KEY] = withBackups;
    request[FILES_KEY]   = QJsonArray::fromStringList(files);

    QJsonDocument document;
    document.setObject(request);

    easyMeta.write(document.toJson(QJsonDocument::Compact));
    easyMeta.write("\n");
    return easyMeta.waitForBytesWritten(TIMEOUT_MS);
}

void receiveResponse(QProcess &easyMeta, QJsonObject &response) {
    QCOMPARE(easyMeta.state(), QProcess::Running);
    QVERIFY(easyMeta.waitForReadyRead(TIMEOUT_MS));
    for (int i = 0; (i < WAIT_PERIOD_COUNT) && !easyMeta.canReadLine(); ++i) {
        std::this_thread::sleep_for(SLEEP_PERIOD);
    }
    QVERIFY(easyMeta.canReadLine());

    QByteArray data       = easyMeta.readLine();
    auto responseDocument = QJsonDocument::fromJson(data);
    QVERIFY(responseDocument.isObject());

    response = responseDocument.object();
}

void readWithExiftool(const QString &path, QJsonDocument &document) {
    QStringList arguments = {"-json",     "-ignoreMinorErrors", "-e",           "-ObjectName",
                             "-Title",    "-ImageDescription",  "-Description", "-Caption-Abstract",
                             "-Keywords", "-Subject",           "-Copyright",   "-Headline",
                             path};

    QProcess exiftool;
#ifdef Q_OS_MAC
    exiftool.start("/usr/local/bin/exiftool", arguments);
#else
    exiftool.start("exiftool", arguments);
#endif

    const bool finished = exiftool.waitForFinished();
    if (!finished) { qWarning() << exiftool.errorString() << exiftool.readAll(); }
    QVERIFY(finished);
    QCOMPARE(exiftool.exitCode(), 0);

    QByteArray output = exiftool.readAllStandardOutput();
    document          = QJsonDocument::fromJson(output);
}

void checkFileErrorResponse(QProcess &easyMeta, const QString &error) {
    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[ERROR_KEY].toString(), error);
}
