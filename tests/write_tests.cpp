/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "write_tests.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QProcess>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void WriteTests::init() {
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("LOGS_DIR", ".");
    m_EasyMeta.setProcessEnvironment(env);
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void WriteTests::cleanup() {
    m_EasyMeta.write("quit\n");
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.terminate();
        if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
            m_EasyMeta.kill();
            QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
        }
    }
}

static bool sendWriteRequest(QProcess &easyMeta,
                             const QStringList &filepaths,
                             const QString &title,
                             const QString &description,
                             const QString &copyright,
                             const QString &headline,
                             const QStringList &keywords,
                             bool withBackups = false,
                             bool viaProxy    = false) {
    if (easyMeta.state() != QProcess::Running) {
        qWarning() << "Easymeta is not running!";
        return false;
    }
    QJsonObject request;
    request[ID_KEY]      = QLatin1String(QTest::currentTestFunction());
    request[TYPE_KEY]    = WRITE_TYPE;
    request[BACKUPS_KEY] = withBackups;
    if (viaProxy) {
        request[WITH_DIRECT_KEY] = false;
        request[WITH_PROXY_KEY]  = true;
    }

    QJsonArray filesArray;
    for (const QString &filepath: qAsConst(filepaths)) {
        QJsonObject fileObject;
        fileObject[SOURCE_FILE_KEY] = filepath;
        fileObject[TITLE_KEY]       = title;
        fileObject[DESCRIPTION_KEY] = description;
        fileObject[COPYRIGHT_KEY]   = copyright;
        fileObject[HEADLINE_KEY]    = headline;
        fileObject[KEYWORDS_KEY]    = QJsonArray::fromStringList(keywords);
        filesArray.append(fileObject);
    }
    request[FILES_KEY] = filesArray;

    QJsonDocument replyDocument;
    replyDocument.setObject(request);

    easyMeta.write(replyDocument.toJson(QJsonDocument::Compact));
    easyMeta.write("\n");
    return easyMeta.waitForBytesWritten(TIMEOUT_MS);
}

static void verifyMetadata(const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QString &copyright,
                           const QString &headline,
                           const QStringList &keywords) {
    QJsonDocument exifResponseDocument;
    readWithExiftool(filepath, exifResponseDocument);
    QVERIFY(!exifResponseDocument.isNull());
    QJsonObject metadata = exifResponseDocument.array()[0].toObject();

    QCOMPARE(metadata["Title"].toString(), title);
    QCOMPARE(metadata["ObjectName"].toString(), title);

    QCOMPARE(metadata["Caption-Abstract"].toString(), description);
    QCOMPARE(metadata["Description"].toString(), description);
    QCOMPARE(metadata["ImageDescription"].toString(), description);

    QCOMPARE(metadata["Copyright"].toString(), copyright);

    QCOMPARE(metadata["Headline"].toString(), headline);

    QCOMPARE(metadata["Keywords"].toArray(), QJsonArray::fromStringList(keywords));
    QCOMPARE(metadata["Subject"].toArray(), QJsonArray::fromStringList(keywords));
}

void WriteTests::badFileEntryTest() {
    QVERIFY(sendWriteRequest(m_EasyMeta, {""}, "title", "descr", "(c)", "headline", {"kw1"}));
    checkFileErrorResponse(m_EasyMeta, "File name is missing or empty");
}

void WriteTests::missingFileTest() {
    QVERIFY(sendWriteRequest(m_EasyMeta, {"no/such/path"}, "title", "descr", "(c)", "headline", {"kw1"}));
    checkFileErrorResponse(m_EasyMeta, "File doesn't exist");
}

void WriteTests::unsupportedFileTest() {
    QString filepath = setupFilePathForTest("tests/data/vector-icon.svg");
    QVERIFY(sendWriteRequest(m_EasyMeta, {filepath}, "title", "descr", "(c)", "headline", {"kw1"}));
    checkFileErrorResponse(m_EasyMeta, "Couldn't update metadata");
}

static void singleFileTest(QProcess &easyMeta,
                           const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QString &copyright,
                           const QString &headline,
                           const QStringList &keywords,
                           bool withBackups,
                           bool viaProxy) {
    QString fileCopy = setupFilePathForTest(filepath);
    QVERIFY(sendWriteRequest(easyMeta, {fileCopy}, title, description, copyright, headline, keywords, withBackups,
                             viaProxy));

    QJsonObject response;
    QThread::msleep(500);
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);
    QJsonObject fileObject = filesArray[0].toObject();
    QCOMPARE(fileObject[SOURCE_FILE_KEY].toString(), fileCopy);
    if (fileObject.contains(ERROR_KEY)) { qWarning() << "Error:" << fileObject[ERROR_KEY]; }
    QVERIFY(!fileObject.contains(ERROR_KEY));

    const QString backupPath = fileCopy + "_original";
    QVERIFY(QFileInfo::exists(backupPath) == withBackups);

    verifyMetadata(fileCopy, title, description, copyright, headline, keywords);
    if (withBackups) { QVERIFY(QFile::remove(backupPath)); }
}

void WriteTests::jpgFileTest() {
    QString title        = "jpgFileTest title";
    QString description  = "jpgFileTest description";
    QString copyright    = "jpgFileTest copyright";
    QString headline     = "jpgFileTest headline";
    QStringList keywords = {"jpgFileTest", "key", "words"};

    bool matrix[] = {false, true};
    for (bool backups: matrix) {
        for (bool proxy: matrix) {
            singleFileTest(m_EasyMeta, {"tests/data/027.jpg"}, title, description, copyright, headline, keywords,
                           backups, proxy);
        }
    }
}

void WriteTests::pngFileTest() {
    QString title        = "pngFileTest title";
    QString description  = "pngFileTest description";
    QString copyright    = "pngFileTest copyright";
    QString headline     = "pngFileTest headline";
    QStringList keywords = {"pngFileTest", "key", "words"};

    singleFileTest(m_EasyMeta,
                   {"tests/data/sample.png"},
                   title,
                   description,
                   copyright,
                   headline,
                   keywords,
                   false /*withBackups*/,
                   false /*proxy*/);
    singleFileTest(m_EasyMeta,
                   {"tests/data/sample.png"},
                   title,
                   description,
                   copyright,
                   headline,
                   keywords,
                   false /*withBackups*/,
                   true /*proxy*/);
}

void WriteTests::rawFileTest() {
    QString title        = "rawFileTest title";
    QString description  = "rawFileTest description";
    QString copyright    = "rawFileTest copyright";
    QString headline     = "rawFileTest headline";
    QStringList keywords = {"rawFileTest", "key", "words"};

    singleFileTest(m_EasyMeta, {"tests/data/5DM35747.dng"}, title, description, copyright, headline, keywords,
                   false /*withBackups*/, false /*proxy*/);
    singleFileTest(m_EasyMeta, {"tests/data/5DM35747.dng"}, title, description, copyright, headline, keywords,
                   false /*withBackups*/, true /*proxy*/);
}

void WriteTests::multipleFilesTest() {
    QStringList filepaths = {setupFilePathForTest("tests/data/026.jpg"), setupFilePathForTest("tests/data/027.jpg")};
    QString title         = "multipleFilesTest title";
    QString description   = "multipleFilesTest description";
    QString copyright     = "multipleFilesTest copyright";
    QString headline      = "multipleFilesTest headline";
    QStringList keywords  = {"multipleFilesTest", "key", "words"};

    QVERIFY(sendWriteRequest(m_EasyMeta, filepaths, title, description, copyright, headline, keywords));

    QJsonObject response;
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 2);
    QJsonObject fileObject = filesArray[0].toObject();
    QVERIFY(filepaths.contains(fileObject[SOURCE_FILE_KEY].toString()));
    QVERIFY(!fileObject.contains(ERROR_KEY));
    fileObject = filesArray[1].toObject();
    QVERIFY(filepaths.contains(fileObject[SOURCE_FILE_KEY].toString()));
    QVERIFY(!fileObject.contains(ERROR_KEY));

    verifyMetadata(filepaths[0], title, description, copyright, headline, keywords);
    verifyMetadata(filepaths[1], title, description, copyright, headline, keywords);
}

static void roundTripTest(QProcess &easyMeta,
                          const QString &filepath,
                          const QString &title,
                          const QString &description,
                          const QString &copyright,
                          const QString &headline,
                          const QStringList &keywords) {
    QVERIFY(sendWriteRequest(easyMeta, {filepath}, title, description, copyright, headline, keywords));

    QJsonObject readResponse;
    receiveResponse(easyMeta, readResponse);
    QVERIFY(!readResponse.isEmpty());

    QVERIFY(sendSimpleRequest(easyMeta, READ_TYPE, {filepath}));
    QJsonObject writeResponse;
    receiveResponse(easyMeta, writeResponse);
    QVERIFY(!writeResponse.isEmpty());

    QVERIFY(writeResponse[FILES_KEY].isArray());
    QJsonArray filesArray = writeResponse[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[TITLE_KEY].toString(), title);
    QCOMPARE(file[DESCRIPTION_KEY].toString(), description);
    QCOMPARE(file[COPYRIGHT_KEY].toString(), copyright);
    QCOMPARE(file[HEADLINE_KEY].toString(), headline);
    QCOMPARE(asString(file[KEYWORDS_KEY].toArray()), asString(keywords));
}

void WriteTests::asciiRoundTripTest() {
    QString filepath     = setupFilePathForTest("tests/data/clear.jpg");
    QString title        = "asciiRoundTripTest title";
    QString description  = "asciiRoundTripTest description";
    QString copyright    = "asciiRoundTripTest copyright";
    QString headline     = "asciiRoundTripTest headline";
    QStringList keywords = {"ascii", "round-trip", "test", "tags"};

    // this creates metadata for the first time
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, headline, keywords);
    // this updates existing metadata
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, headline, keywords);
}

void WriteTests::unicodeRoundTripTest() {
    QString filepath     = setupFilePathForTest("tests/data/clear.jpg");
    QString title        = "πύργος του Άιφελ";
    QString description  = "První plány stavby byly zahájeny už v roce 1878.";
    QString copyright    = "Все права сохранены";
    QString headline     = "Die Tür ist rot.";
    QStringList keywords = {"buokšts", "sėmbuolu", "Parīžiuo", "aukštliausės", "bodīnks",
                            "metās",   "Е́йфелева", "ве́жа",     "埃菲尔铁塔"};

    // this creates metadata for the first time
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, headline, keywords);
    // this updates existing metadata
    roundTripTest(m_EasyMeta, filepath, title, description, copyright, headline, keywords);
}
