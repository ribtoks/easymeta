/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "read_tests.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void ReadTests::init() {
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("LOGS_DIR", ".");
    m_EasyMeta.setProcessEnvironment(env);
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void ReadTests::cleanup() {
    m_EasyMeta.write("quit\n");
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.terminate();
        if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
            m_EasyMeta.kill();
            QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
        }
    }
}

static void singleFileTest(QProcess &easyMeta,
                           const QString &filepath,
                           const QString &title,
                           const QString &description,
                           const QString &copyright,
                           const QString &headline,
                           const QStringList &keywords) {
    QVERIFY(sendSimpleRequest(easyMeta, READ_TYPE, {findFullPathForTests(filepath)}));

    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[ERROR_KEY].toString(), QString(""));
    QCOMPARE(file[TITLE_KEY].toString(), title);
    QCOMPARE(file[DESCRIPTION_KEY].toString(), description);
    QCOMPARE(file[COPYRIGHT_KEY].toString(), copyright);
    QCOMPARE(file[HEADLINE_KEY].toString(), headline);
    QCOMPARE(asString(file[KEYWORDS_KEY].toArray()), asString(keywords));
    QVERIFY(file[CREATED_AT_KEY].isString());
}

void ReadTests::jpgFileTest() {
    QStringList keywords = {"abstract",   "art",        "black",    "blue",     "creative", "dark",     "decor",
                            "decoration", "decorative", "design",   "dot",      "drops",    "elegance", "element",
                            "geometric",  "interior",   "light",    "modern",   "old",      "ornate",   "paper",
                            "pattern",    "purple",     "retro",    "seamless", "style",    "textile",  "texture",
                            "vector",     "wall",       "wallpaper"};

    singleFileTest(m_EasyMeta, "tests/data/026.jpg", "background droplets",
                   "background droplets vector illustration with textures", "" /*copyright*/, "" /*headline*/,
                   keywords);
}

void ReadTests::pngFileTest() {
    QStringList keywords = {"basic", "png", "keywords", "support"};

    singleFileTest(m_EasyMeta, "tests/data/sample.png", "", "Created with GIMP", "", "", keywords);
}

static void checkReadFileErrorResponse(QProcess &easyMeta, const QString &error) {
    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());

    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QVERIFY(!file.contains(TITLE_KEY));
    QVERIFY(!file.contains(DESCRIPTION_KEY));
    QVERIFY(!file.contains(COPYRIGHT_KEY));
    QVERIFY(!file.contains(HEADLINE_KEY));
    QVERIFY(!file.contains(KEYWORDS_KEY));
    QCOMPARE(file[ERROR_KEY].toString(), error);
}

void ReadTests::badFileEntryTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, {""}));
    checkReadFileErrorResponse(m_EasyMeta, "File name is missing or empty");
}

void ReadTests::missingFileTest() {
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, {"no/such/file"}));
    checkReadFileErrorResponse(m_EasyMeta, "File doesn't exist");
}

void ReadTests::unsupportedFileTest() {
    QStringList files = {findFullPathForTests("tests/data/vector-icon.svg")};
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, files));
    checkReadFileErrorResponse(m_EasyMeta, "Couldn't read metadata");
}

void ReadTests::multipleFilesTest() {
    QStringList files = {findFullPathForTests("tests/data/026.jpg"), findFullPathForTests("tests/data/027.jpg")};
    QVERIFY(sendSimpleRequest(m_EasyMeta, READ_TYPE, files));

    QJsonObject response;
    receiveResponse(m_EasyMeta, response);
    QVERIFY(!response.isEmpty());

    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 2);
    for (const QJsonValue file: qAsConst(filesArray)) {
        QVERIFY(file.isObject());
        QJsonObject fileObject = file.toObject();
        QVERIFY(files.contains(fileObject[SOURCE_FILE_KEY].toString()));
    }
}

void ReadTests::legacyFormatTest() {
    QStringList expectedKeywords =
        QString(
            "rock,nature,landscape,white,background,beautiful,sun,light,mountain,outdoor,top,rocky,snow,fog,horizon")
            .split(',');
    singleFileTest(m_EasyMeta, "tests/data/img_0007.jpg", "Half of mountain range in clouds",
                   "Half of mountain range in clouds", "" /*copyright*/, "" /*headline*/, expectedKeywords);
}
