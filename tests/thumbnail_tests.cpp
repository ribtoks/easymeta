/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "thumbnail_tests.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

bool sendThumbnailRequest(QProcess &easyMeta, const QString &dir, const QStringList &files) {
    QJsonObject request;
    request[ID_KEY]      = QLatin1String(QTest::currentTestFunction());
    request[TYPE_KEY]    = THUMBNAIL_TYPE;
    request[OUT_DIR_KEY] = dir;

    QJsonArray filesArray;
    for (const QString &file: files) {
        QJsonObject fileObject;
        fileObject[SOURCE_FILE_KEY]  = file;
        fileObject[OUT_FILENAME_KEY] = QFileInfo(file).baseName();
        filesArray.append(fileObject);
    }
    request[FILES_KEY] = filesArray;

    QJsonDocument document;
    document.setObject(request);

    easyMeta.write(document.toJson(QJsonDocument::Compact));
    easyMeta.write("\n");
    return easyMeta.waitForBytesWritten(TIMEOUT_MS);
}

static void
singleThumbnailTest(QProcess &easyMeta, const QString &dir, const QString &filepath, const QString &extension) {
    QVERIFY(sendThumbnailRequest(easyMeta, dir, {findFullPathForTests(filepath)}));

    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));

    QVERIFY(response[FILES_KEY].isArray());
    QJsonArray filesArray = response[FILES_KEY].toArray();
    QCOMPARE(filesArray.size(), 1);

    QVERIFY(filesArray[0].isObject());
    QJsonObject file = filesArray[0].toObject();
    QCOMPARE(file[ERROR_KEY].toString(), QString(""));
    QCOMPARE(file[EXTENSION_KEY].toString(), extension);

    QFileInfo fi(QDir::cleanPath(dir + '/' + QFileInfo(filepath).baseName() + extension));
    QVERIFY(fi.exists());
    QVERIFY(fi.size() > 0);
}

void ThumbnailTests::init() {
    bool isValid = m_Dir.isValid();
    Q_UNUSED(isValid);
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void ThumbnailTests::cleanup() {
    m_EasyMeta.terminate();
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.kill();
        QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
    }
    m_Dir.remove();
}

void ThumbnailTests::vectorThumbnailTest() {
    singleThumbnailTest(m_EasyMeta, m_Dir.path(), "tests/data/026.eps", ".tif");
}
