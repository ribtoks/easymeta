/*
 * This file is a part of EasyMeta - cross-platform application
 * for basic metadata editing in photos, vectors and videos
 * Copyright (C) 2020-2023 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "generic_tests.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QStringList>

#include "Helpers/constants.h"

#include "testhelpers.h"

void GenericTests::init() {
    m_EasyMeta.start("../src/easymeta", QStringList());
    QVERIFY(m_EasyMeta.waitForStarted(TIMEOUT_MS));
}

void GenericTests::cleanup() {
    m_EasyMeta.terminate();
    if (!m_EasyMeta.waitForFinished(TIMEOUT_MS)) {
        m_EasyMeta.kill();
        QVERIFY(m_EasyMeta.waitForFinished(TIMEOUT_MS));
    }
}

void GenericTests::missingIdTest() {
    QJsonObject request;
    request[TYPE_KEY] = READ_TYPE;

    QJsonDocument document;
    document.setObject(request);

    m_EasyMeta.write(document.toJson(QJsonDocument::Compact));
    m_EasyMeta.write("\n");
    QVERIFY(m_EasyMeta.waitForBytesWritten(TIMEOUT_MS));
    QVERIFY(!m_EasyMeta.waitForReadyRead(TIMEOUT_MS));
}

static void errorTest(QProcess &easyMeta, const QJsonObject &request, const QString &error) {
    QJsonDocument document;
    document.setObject(request);

    easyMeta.write(document.toJson(QJsonDocument::Compact));
    easyMeta.write("\n");
    QVERIFY(easyMeta.waitForBytesWritten(TIMEOUT_MS));

    QJsonObject response;
    receiveResponse(easyMeta, response);
    QVERIFY(!response.isEmpty());
    QCOMPARE(response[ID_KEY].toString(), QString(QLatin1String(QTest::currentTestFunction())));
    QCOMPARE(response[ERROR_KEY].toString(), error);
}

void GenericTests::missingTypeTest() {
    QJsonObject request;
    request[ID_KEY] = QLatin1String(QTest::currentTestFunction());
    errorTest(m_EasyMeta, request, "Unknown request type: ");
}

void GenericTests::unknownTypeTest() {
    QJsonObject request;
    request[ID_KEY]   = QLatin1String(QTest::currentTestFunction());
    request[TYPE_KEY] = "type";
    errorTest(m_EasyMeta, request, "Unknown request type: type");
}
